﻿using System;
using System.Linq;

namespace WorkingWithArrays
{
    public static class UsingRanges
    {
        public static int[] GetArrayWithAllElements(int[] array)
        {
            int[] copied = (from elem in array select elem).ToArray();
            return copied;
        }

        public static int[] GetArrayWithoutFirstElement(int[] array)
        {
            int[] copied = (from elem in array where elem != array[0] select elem).ToArray();
            return copied;
        }

        public static int[] GetArrayWithoutTwoFirstElements(int[] array)
        {
            int[] copied = (from elem in array where elem != array[0] && elem != array[1] select elem).ToArray();
            return copied;
        }

        public static int[] GetArrayWithoutThreeFirstElements(int[] array)
        {
            int[] copied = (from elem in array where elem != array[0] && elem != array[1] && elem != array[2] select elem).ToArray();
            return copied;
        }

        public static int[] GetArrayWithoutLastElement(int[] array)
        {
            int[] copied = (from elem in array where elem != array[array.Length - 1] select elem).ToArray();
            return copied;
        }

        public static int[] GetArrayWithoutTwoLastElements(int[] array)
        {
            int[] copied = (from elem in array where elem != array[array.Length - 1] && elem != array[array.Length - 2] select elem).ToArray();
            return copied;
        }

        public static int[] GetArrayWithoutThreeLastElements(int[] array)
        {
            int[] copied = (from elem in array where elem != array[array.Length - 1] && elem != array[array.Length - 2] && elem != array[array.Length - 3] select elem).ToArray();
            return copied;
        }

        public static bool[] GetArrayWithoutFirstAndLastElements(bool[] array)
        {
            bool[] copied = (from elem in array where elem != array[array.Length - 1] && elem != array[0] select elem).ToArray();
            return copied;
        }

        public static bool[] GetArrayWithoutTwoFirstAndTwoLastElements(bool[] array)
        {
            bool[] copied = (from elem in array where elem != array[array.Length - 1] && elem != array[array.Length - 2] && elem != array[0] && elem != array[1] select elem).ToArray();
            return copied;
        }

        public static bool[] GetArrayWithoutThreeFirstAndThreeLastElements(bool[] array)
        {
            bool[] copied = (from elem in array where elem != array[array.Length - 1] && elem != array[array.Length - 2] && elem != array[array.Length - 3] && elem != array[0] && elem != array[1] && elem != array[2] select elem).ToArray();
            return copied;
        }
    }
}
